#include "uart.h"
#include "my_tcb_list.h"
#include "scheduler.h"
#include <avr/io.h>
#include <util/delay.h>
#include <avr/interrupt.h>
#include <stdio.h>
#include <stdbool.h>

#define BAUD 19600
#define MYUBRR F_CPU/16/BAUD-1

void usart_init(uint16_t ubrr);
char usart_getchar( void );
void usart_putchar( char data );
void usart_pstr (char *s);
unsigned char usart_kbhit(void);
int usart_putchar_printf(char var, FILE *stream);

char bufferRX[1];


static FILE mystdout = FDEV_SETUP_STREAM(usart_putchar_printf, NULL, _FDEV_SETUP_WRITE);


// interrupt di ricezione carattere

ISR(USART0_RX_vect){

  bufferRX[0]=UDR0; 
  schedule(2);
}  



void usart_init( uint16_t ubrr) {

    UBRR0H = (uint8_t)(ubrr>>8);
    UBRR0L = (uint8_t)ubrr;

    UCSR0C = _BV(UCSZ01) | _BV(UCSZ00); 
    UCSR0B = _BV(RXEN0) | _BV(TXEN0) | _BV(RXCIE0);    
}
void usart_putchar(char data) {
   
    while ( !(UCSR0A & (_BV(UDRE0))) );
    
    UDR0 = data; 
}
char usart_getchar(void) {
   
    while ( !(UCSR0A & (_BV(RXC0))) );
 
    return UDR0;
}
unsigned char usart_kbhit(void) {

    unsigned char b;
    b=0;
    if(UCSR0A & (1<<RXC0)) b=1;
    return b;
}
void usart_pstr(char *s) {
    while (*s) { 
        usart_putchar(*s);
        s++;
    }
}
 
 
 void putChar(char c){
 
	int is_buffer_full=0;
	
	while ( !(UCSR0A & (_BV(UDRE0))) && is_buffer_full < 1){
		
	  is_buffer_full++;
	}
			
	if(is_buffer_full==1){

	  schedule(3); 
	  return;
	}
	
	else {

	  UDR0 = c;
	}
}

char getChar(void){

	int not_est_dato=0;

	while( !(bufferRX[0]) && not_est_dato < 1 ) {
		
	  not_est_dato++;
	}
	
	if(not_est_dato == 1){
	
	  schedule(1);
	  
	    
	}
	
	char r = bufferRX[0];
	bufferRX[0] = '\0';

	return r;
}


 
 
int usart_putchar_printf(char var, FILE *stream) {
  
    if (var == '\n') usart_putchar('\r');
    usart_putchar(var);
    return 0;
}

void printf_init(void){
  stdout = &mystdout;
  
  usart_init ( MYUBRR );
}
