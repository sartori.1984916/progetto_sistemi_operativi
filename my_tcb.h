#pragma once
#include <stdint.h>
#include <stddef.h>

#define OK     0
#define ERROR -1

typedef uint8_t* Pointer;
typedef void (* ThreadFn)(uint32_t thread_args);

typedef enum {Running=0x0, Terminated=0x1, Ready=0x2, Waiting= 0x3} ThreadStatus;


typedef struct my_TCB {

  Pointer sp_save_ptr;
  
  ThreadFn thread_fn;
  uint32_t thread_arg;

  struct my_TCB* next;
  struct my_TCB* prev;
  
  Pointer stack_bottom;         
  uint32_t stack_size;         
  ThreadStatus status;
  
  
} my_TCB;


void my_TCB_create(my_TCB* tcb, Pointer stack_top, ThreadFn thread_fn, uint32_t thread_arg);

