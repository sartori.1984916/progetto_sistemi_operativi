#include <avr/interrupt.h>
#include <avr/io.h>
#include <stdio.h>
#include <assert.h>
#include <util/delay.h>
#include <string.h>
#include "my_tcb.h"
#include "my_tcb_list.h"
#include "atomport_asm.h"
#include "timer.h"
#include "scheduler.h"
#include "interrupt.h"


my_TCB* current_tcb;

my_Scheduler* scheduler;

extern char* ex_buffer_1;
extern char* ex_buffer_2;
extern char* ex_buffer_3;

int timer_count = 0;


my_TCBList ready_queue_rx={
  .first=NULL,
  .last=NULL,
  .size=0
};

my_TCBList ready_queue_tx={
  .first=NULL,
  .last=NULL,
  .size=0
};

my_TCBList running_queue={
  .first=NULL,
  .last=NULL,
  .size=0
  };


void startSchedule(void){

  cli();
  
  scheduler = (my_Scheduler*)malloc(sizeof(my_Scheduler));
  current_tcb = (my_TCB*)malloc(sizeof(my_TCB));
  
  scheduler-> ready_queue_rx = ready_queue_rx;
  scheduler-> ready_queue_tx = ready_queue_tx;
  scheduler->running_queue = running_queue;

  current_tcb = my_TCBList_dequeue(&(scheduler->running_queue));
  
  my_TCBList_enqueue(&(scheduler->running_queue), current_tcb);     // per avere Sleep sempre a disposizione in running_queue
  current_tcb->status = Running; 
  
  assert(current_tcb);
  
  timerStart();
  _init_interrupt();
  
  archFirstThreadRestore(current_tcb);
}

void schedule(int code) {

  my_TCB* old_tcb = current_tcb;
  
  if (code == 3){      // un thread vuole scrivere ma buffer output pieno, si mette in running Sleep
  
    current_tcb->status = Waiting;  
    my_TCBList_enqueue(&(scheduler->ready_queue_tx), current_tcb);
    
    current_tcb = my_TCBList_dequeue(&(scheduler->running_queue));
    my_TCBList_enqueue(&(scheduler->running_queue), current_tcb);  
          											     
    current_tcb->status= Running;
  
    if (old_tcb!=current_tcb){
    
    //printf("\ncod3\n");
     // _delay_ms(1000);
      archContextSwitch(old_tcb, current_tcb);
      
      }
      
   }	
  
  if (code == 1){	    // un thread vuole ricevere ma input buffer vuoto, output buffer vuoto: si mette in running 			    //  un processo che scrive
    current_tcb->status= Waiting;
    my_TCBList_enqueue(&(scheduler->ready_queue_rx), current_tcb);
    
    current_tcb= my_TCBList_dequeue(&(scheduler->ready_queue_tx));
    current_tcb->status= Running;
     
  
    if (old_tcb!= current_tcb){
    
     // printf("cod1\n");
      //_delay_ms(1000); 
      archContextSwitch(old_tcb, current_tcb);
      
      }
      
   }
            
  
  if (code == 2) {	    // arrivo di un dato: input buffer pieno, si mette in running un processo che riceve

    current_tcb->status= Ready;
  
    current_tcb= my_TCBList_dequeue(&(scheduler->ready_queue_rx));
    current_tcb->status= Running;                                                                        
  
    if (old_tcb!= current_tcb){
    //printf("\ncod2\n");
    // _delay_ms(1000);
      archContextSwitch(old_tcb, current_tcb);
      }
      
    }
  

}






void print_status(void){

 timer_count+=1;
 
 if (timer_count % 15 == 0) {
	
         printf("\n\n");
	  
	  printf("READY QUEUE dei processi di lettura: \n");
	  my_TCBList_print(&(scheduler->ready_queue_rx));
	  
	  printf("\n");
	  
	  printf("READY QUEUE dei processi di scrittura: \n");
	  my_TCBList_print(&(scheduler->ready_queue_tx));
	  
	  printf("\n");
	  
	  printf("CURRENT PROCESS: %p\n", current_tcb);
	  
	  printf("\n");
	  
	  printf("Status Exchange Buffer 1: \n");
	  
	  printf("%s\n", ex_buffer_1);
	  
	  printf("Status Exchange Buffer 2: \n");
	  
	  printf("%s\n", ex_buffer_2);
	  
	  printf("Status Exchange Buffer 3: \n");
	  
	  printf("%s", ex_buffer_3);
	  
	  printf("\n\n");
  
  }


}
