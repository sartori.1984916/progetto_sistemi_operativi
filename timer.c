#include "timer.h"
#include "scheduler.h"
  

void timerStart ( void )
{  
    
    OCR1A = (AVR_CPU_HZ / 256 / SYSTEM_TICKS_PER_SEC);
    
    // OCR value impostato tale da generare evento ogni 1s (SYSTEM_TICKS_PER_SEC = 1)

    
#ifdef TIMSK
    TIMSK = _BV(OCIE1A);
#else
    TIMSK1 = _BV(OCIE1A);
#endif

   
    TCCR1B = _BV(CS12) | _BV(WGM12);
}


// timer interrupt
ISR (TIMER1_COMPA_vect)
{
 print_status();
}

