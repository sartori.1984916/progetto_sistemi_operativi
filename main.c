#include <avr/interrupt.h>
#include <avr/io.h>
#include <assert.h>
#include <util/delay.h>
#include <stdint.h>
#include <stdio.h>
#include "my_tcb.h"
#include "my_tcb_list.h"
#include "uart.h"
#include "atomport_asm.h"
#include "scheduler.h"
#define THREAD_STACK_SIZE 256
#define IDLE_STACK_SIZE 128
#define EXCHANGE_BUFFER_SIZE 2
#define MAX_TCB_RX 6
#define MAX_TCB_TX 6


my_TCB p6_tcb;
my_TCB p5_tcb;
my_TCB p4_tcb;
my_TCB p3_tcb;
my_TCB p2_tcb;
my_TCB p1_tcb;

my_TCB sleep;



uint8_t p6_stack[THREAD_STACK_SIZE];
uint8_t p5_stack[THREAD_STACK_SIZE];
uint8_t p4_stack[THREAD_STACK_SIZE];
uint8_t p3_stack[THREAD_STACK_SIZE];
uint8_t p2_stack[THREAD_STACK_SIZE];
uint8_t p1_stack[THREAD_STACK_SIZE];

uint8_t sleep_stack[IDLE_STACK_SIZE];

char* ex_buffer_1;
char* ex_buffer_2;
char* ex_buffer_3;



void p6_fn(uint32_t arg __attribute__((unused))){

  while(1){	
  
    putChar(*ex_buffer_3);
    
  }
}   

void p5_fn(uint32_t arg __attribute__((unused))){

  while(1){	
  
    putChar(*ex_buffer_2);
    
  }
}  

void p4_fn(uint32_t arg __attribute__((unused))){

  while(1){	
  
    putChar(*ex_buffer_1);
    
  }
}        




void p3_fn(uint32_t arg __attribute__((unused))){  

 
 //printf("p1\n"); 
 //_delay_ms(1000);
 
 char c;
         
 while(1){                         
         
    c = getChar(); 
         
    if (c != '\0' ) {
       *ex_buffer_3 = c-1;
     } 
  }
}  

void p2_fn(uint32_t arg __attribute__((unused))){  

 
 //printf("p1\n"); 
 //_delay_ms(1000);
 
 char c;
         
 while(1){                         
         
    c = getChar(); 
         
    if (c != '\0' ) {
       
       *ex_buffer_2 = c+1;
     } 
  }
}  

void p1_fn(uint32_t arg __attribute__((unused))){  

 
 //printf("p1\n"); 
 //_delay_ms(1000);
 
 char c;
         
 while(1){                         
         
    c = getChar(); 
         
    if (c != '\0' ) {
    
       *ex_buffer_1 = c;
     }
       
  }
}                         

void sleep_fn(uint32_t arg __attribute__((unused))) {

 while(1){
  
   _delay_ms(5000);
     printf("\nsleeping...\n");}
}




int main(void){
  
  printf_init();
 
  ex_buffer_1 = (char*)malloc(sizeof(char)*EXCHANGE_BUFFER_SIZE);
  ex_buffer_2 = (char*)malloc(sizeof(char)*EXCHANGE_BUFFER_SIZE);
  ex_buffer_3 = (char*)malloc(sizeof(char)*EXCHANGE_BUFFER_SIZE);
  
  my_TCB_create(&p6_tcb, p6_stack+THREAD_STACK_SIZE-1, p6_fn, 0);
  my_TCB_create(&p5_tcb, p5_stack+THREAD_STACK_SIZE-1, p5_fn, 0);
  my_TCB_create(&p4_tcb, p4_stack+THREAD_STACK_SIZE-1, p4_fn, 0);
  my_TCB_create(&p3_tcb, p3_stack+THREAD_STACK_SIZE-1, p3_fn, 0);
  my_TCB_create(&p2_tcb, p2_stack+THREAD_STACK_SIZE-1, p2_fn, 0);
  my_TCB_create(&p1_tcb, p1_stack+THREAD_STACK_SIZE-1, p1_fn, 0);
  
  my_TCB_create(&sleep, sleep_stack+IDLE_STACK_SIZE-1, sleep_fn, 0);

  my_TCBList_enqueue(&ready_queue_rx, &p1_tcb);
  my_TCBList_enqueue(&ready_queue_rx, &p2_tcb);
  my_TCBList_enqueue(&ready_queue_rx, &p3_tcb);
  my_TCBList_enqueue(&ready_queue_tx, &p4_tcb);
  my_TCBList_enqueue(&ready_queue_tx, &p5_tcb);
  my_TCBList_enqueue(&ready_queue_tx, &p6_tcb);
  
  my_TCBList_enqueue(&running_queue, &sleep);
  
																							
  printf("\nstarting\n");
  startSchedule();
}
