#include "my_tcb.h"
#include "my_tcb_list.h"



typedef struct my_Scheduler {
  
  my_TCBList ready_queue_rx;
  my_TCBList ready_queue_tx;
  
  my_TCBList running_queue;
  
  
} my_Scheduler;


void startSchedule(void);
void print_status(void);

void schedule(int code);
     
extern my_TCBList running_queue;
extern my_TCBList ready_queue_tx;
extern my_TCBList ready_queue_rx;
extern my_TCB* current_tcb;


extern char* ex_buffer_1;
extern char* ex_buffer_2;
extern char* ex_buffer_3;
extern my_Scheduler* scheduler;
