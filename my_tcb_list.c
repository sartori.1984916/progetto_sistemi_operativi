#include "my_tcb.h"
#include "my_tcb_list.h"
#include <stdio.h>

// pops the first thread from the queue, and detaches it
my_TCB* my_TCBList_dequeue(my_TCBList* list){
  my_TCB* tcb=list->first;
  if (tcb==NULL)
    return tcb;
  
  if (list->size==1) {
    list->first = list->last = NULL;
  } else {
    my_TCB* next= tcb->next;
    list->first=next;
    next->prev=next;
  }
  --list->size;
  tcb->next=NULL;
  tcb->prev=NULL;
  return tcb;
}

// adds a thread to the end of the queue
// thread must be detached
uint8_t my_TCBList_enqueue(my_TCBList* list, my_TCB* tcb){
  if(tcb->prev!=NULL || tcb->next!=NULL)
    return ERROR;
  
  if (!list->size) {
    list->first=tcb;
    list->last=tcb;
    tcb->prev=tcb;
    tcb->next=NULL;
  } else {
    list->last->next=tcb;
    tcb->prev=list->last;
    tcb->next=NULL;
    list->last=tcb;
  }
  ++list->size;
  return OK;
}

void my_TCBList_print(my_TCBList* list){
  my_TCB* aux=list->first;
  printf("start-list\n");
  while(aux!=NULL){
    printf("[c: %p, p: %p, n: %p]\n ",
           aux, aux->prev, aux->next);
    aux=aux->next;
  }
  printf("end-list\n");
}
