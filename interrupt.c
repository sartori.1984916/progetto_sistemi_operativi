#include <avr/interrupt.h>
#include <avr/io.h>
#include "interrupt.h"
#include "timer.h"
#include "scheduler.h"
#include <stdio.h>


void _init_interrupt(){	

  cli();
  EICRB |= 0x0C;          // abilita External Interrupt (da datasheet)
  EIMSK |= TRIG_MASK;
  DDRE |= TRIG_MASK;
  sei();
}






