#include "my_tcb.h"
#include "my_tcb_list.h"
#include "scheduler.h"
#include <stdio.h>
#include <avr/interrupt.h>

extern my_TCB* current_tcb;


static void _trampoline(void){

  sei();
  
  if (current_tcb && current_tcb->thread_fn) {
    (*current_tcb->thread_fn)(current_tcb->thread_arg);
  }

  current_tcb->status=Terminated;
}

void my_TCB_create(my_TCB* tcb, Pointer stack_top, ThreadFn thread_fn, uint32_t thread_arg){

  tcb->thread_fn=thread_fn;
  tcb->thread_arg=thread_arg;
  tcb->prev=NULL;
  tcb->next=NULL;
  tcb->status=Ready;
  
 
 
  
  uint8_t *stack_ptr = (uint8_t *)stack_top;

 
  *stack_ptr-- = (uint8_t)((uint16_t)_trampoline & 0xFF);
  *stack_ptr-- = (uint8_t)(((uint16_t)_trampoline >> 8) & 0xFF);

 
#ifdef __AVR_3_BYTE_PC__
  *stack_ptr-- = 0;
#endif


  
  *stack_ptr-- = 0x00;    /* R2 */
  *stack_ptr-- = 0x00;    /* R3 */
  *stack_ptr-- = 0x00;    /* R4 */
  *stack_ptr-- = 0x00;    /* R5 */
  *stack_ptr-- = 0x00;    /* R6 */
  *stack_ptr-- = 0x00;    /* R7 */
  *stack_ptr-- = 0x00;    /* R8 */
  *stack_ptr-- = 0x00;    /* R9 */
  *stack_ptr-- = 0x00;    /* R10 */
  *stack_ptr-- = 0x00;    /* R11 */
  *stack_ptr-- = 0x00;    /* R12 */
  *stack_ptr-- = 0x00;    /* R13 */
  *stack_ptr-- = 0x00;    /* R14 */
  *stack_ptr-- = 0x00;    /* R15 */
  *stack_ptr-- = 0x00;    /* R16 */
  *stack_ptr-- = 0x00;    /* R17 */
  *stack_ptr-- = 0x00;    /* R28 */
  *stack_ptr-- = 0x00;    /* R29 */

    
#ifdef __AVR_3_BYTE_PC__
  *stack_ptr-- = 0x00;    
  *stack_ptr-- = 0x00;    
#endif

  tcb->sp_save_ptr = stack_ptr;

}






