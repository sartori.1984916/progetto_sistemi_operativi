#pragma once
#include "my_tcb.h"

//prototype for assembly functions
void archContextSwitch (my_TCB *old_tcb_ptr, my_TCB *new_tcb_ptr);
void archFirstThreadRestore(my_TCB *new_tcb_ptr);

