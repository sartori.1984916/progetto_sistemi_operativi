#pragma once
#include "my_tcb.h"


typedef struct {
  struct my_TCB* first;
  struct my_TCB* last;
  uint8_t size;
} my_TCBList;



my_TCB* my_TCBList_dequeue(my_TCBList* list);


uint8_t my_TCBList_enqueue(my_TCBList* list, my_TCB* tcb);


void my_TCBList_print(my_TCBList* list);
