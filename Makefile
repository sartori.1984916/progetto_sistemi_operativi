# set this to false to disable sonars in firmware
CC=avr-gcc
AS=avr-gcc
CXX=g++
INCLUDE_DIRS=-I.
CC_OPTS=-Wall --std=gnu99 -DF_CPU=16000000UL -O3 -funsigned-char -funsigned-bitfields  -fshort-enums -Wall -Wstrict-prototypes -mmcu=atmega2560 $(INCLUDE_DIRS)  -D__AVR_3_BYTE_PC__
AS_OPTS=-x assembler-with-cpp $(CC_OPTS)

AVRDUDE=avrdude

# com1 = serial port. Use lpt1 to connect to parallel port.
AVRDUDE_PORT = /dev/ttyACM0    # programmer connected to serial device

AVRDUDE_WRITE_FLASH = -U flash:w:$(TARGET):i
AVRDUDE_FLAGS = -p m2560 -P $(AVRDUDE_PORT) -c $(AVRDUDE_PROGRAMMER) -b 115200
AVRDUDE_FLAGS += $(AVRDUDE_NO_VERIFY)
AVRDUDE_FLAGS += $(AVRDUDE_VERBOSE)
AVRDUDE_FLAGS += $(AVRDUDE_ERASE_COUNTER)
AVRDUDE_FLAGS += -D -q -V -C /usr/share/arduino/hardware/tools/avr/../avrdude.conf
AVRDUDE_FLAGS += -c wiring


OBJS=uart.o\
     my_tcb.o\
     my_tcb_list.o\
     atomport_asm.o\
     timer.o\
     interrupt.o\
     scheduler.o

HEADERS=uart.h\
        my_tcb.h\
	my_tcb_list.h\
	atomport_asm.h\
	timer.h\
        interrupt.h\
        scheduler.h\
        serial_linux.h

BINS= main.elf  serial_linux

.phony:	clean all

all:	$(BINS) 

#common objects
%.o:	%.c 
	$(CC) $(CC_OPTS) -c  $<

%.o:	%.s 
	$(AS) $(AS_OPTS) -c  $<

%.elf:	%.o $(OBJS)
	$(CC) $(CC_OPTS) -o $@ $< $(OBJS) $(LIBS)


%.hex:	%.elf
	avr-objcopy -O ihex -R .eeprom $< $@
	$(AVRDUDE) $(AVRDUDE_FLAGS) -U flash:w:$@:i #$(AVRDUDE_WRITE_EEPROM)


serial_linux:	serial_linux.cpp
	$(CXX) -$(CCOPTS) -o $@ $^

clean:	
	rm -rf $(OBJS) $(BINS) *.hex *~ *.o
